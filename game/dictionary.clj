(ns game.dictionary
  (:require [clojure.pprint]
            [clojure.reflect]
            [clojure.edn])
  (:use [arcadia.core]
        [arcadia.linear])
  (:import [Godot]))

(defn quick-reflect [arg]
  (clojure.pprint/pprint (map second (map first (:members (clojure.reflect/reflect arg))))))

(defn full-reflect [arg]
  (clojure.pprint/pprint (clojure.reflect/reflect arg)))

(defn set-vector! [arg1 arg2]
  (set! (.x arg1) (.x arg2))
  (set! (.y arg1) (.y arg2))
  (set! (.z arg1) (.z arg2)))

(defn mult-vector [arg1 arg2]
  (set! (.x arg1) (float (* (.x arg1) arg2)))
  (set! (.y arg1) (float (* (.y arg1) arg2)))
  (set! (.z arg1) (float (* (.z arg1) arg2)))
  arg1)

(defn translate-transform [obj1 vector1]
  (set! (.Transform obj1) (Transform. Basis/Identity vector1)))
