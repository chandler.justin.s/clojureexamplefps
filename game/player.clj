(ns game.player
  (:require [game.core])
  (:use [arcadia.core]
        [arcadia.linear]
        [game.dictionary])
  (:import [Godot]))

(def ^:dynamic *angle* 0)
(def ^:dynamic *velocity* (v3 0 0 0))
(def ^:dynamic *direction* (v3 0 0 0))

(def sensit 0.3)
(def walk-speed 7.0)
(def run-speed 14.0)
(def accel 6.0)
(def decel 12.0)
(def airaccel 1.0)
(def jump-height 8)
(def gravity (* -9.8 2))
(def player-cam (find-node "ClippedCamera"))
(def player (find-node "ClojurePlayer"))
(def walk-sound (find-node "WalkSounds"))
(def run-sound (find-node "RunSounds"))

;;Check if obj is on the floor. Obj must be a kinematic body
(defn grounded? [obj]
  (if (true? (.IsOnFloor obj))
    true
    false))

;;Takes in the player camera, grabs the kinematic body parent from the hierarchy
(defn get-kinematic [obj]
  (parent (parent obj)))

;;Determine our acceleration speed. Less control in the air, decelerate if on the ground and no input,
;;regular acceleration if on the ground with input. Arbitrarily takes in the script attachment, i.e. the player camera,
;;to grab the kinematic body for the grounded? function
(defn acceleration? [this]
  (if (grounded? (get-kinematic this))
      (if (> (float 0) (float (.Dot *direction* (v3 (.x *velocity*) (float 0) (.z *velocity*)))))
        accel
        decel)
    airaccel))

;;Check sprint button
(defn speed? []
  (if (Input/IsActionPressed "move_sprint")
    run-speed
    walk-speed))

;;Reset player to the default position. Mainly for testing
(defn reset-player []
  (translate-transform player (v3 3 3 0)))

;;Physics and movement input loop
(defn physics-process [this _ delta]
  (set-vector! *direction* (v3 0 0 0)) ;;Reset direction every frame
  (def ^:dynamic *aim* (.basis (.Transform (get-kinematic this)))) ;;Get where the kinematic body is aiming, not the camera
  ;;This is important because if we use the camera aim, a person can walk through the air by looking up.

  ;;Directional inputs - Arrows, WASD
  (if (Input/IsActionPressed "ui_up")
      (do
        (set-vector! *direction* (v3- *direction* (.z *aim*)))))

  (if (Input/IsActionPressed "ui_down")
      (do
        (set-vector! *direction* (v3+ *direction* (.z *aim*)))))

  (if (Input/IsActionPressed "ui_right")
    (do
      (set-vector! *direction* (v3+ *direction* (.x *aim*)))))

  (if (Input/IsActionPressed "ui_left")
    (do
      (set-vector! *direction* (v3- *direction* (.x *aim*)))))

  ;;Jump input - Spacebar
  (if (Input/IsActionJustPressed "ui_accept")
    (do
      (if (grounded? (get-kinematic this)) ;;Check if kinematic body is grounded
        (do
          (set! (.y *velocity*) jump-height)))))

  ;;Walk and run noises
  ;; If speed is greater than 1.0 and on the ground, check whether we're walking or running and turn up that speaker
  (if (and (> (.Length (v3 (.x *velocity*) 0.0 (.z *velocity*))) 1.0) (= (grounded? (get-kinematic this)) true))
    (if (= (speed?) walk-speed)
      (do
        (set! (.VolumeDb walk-sound) -3)
        (set! (.VolumeDb run-sound) -80))
      (do
        (set! (.VolumeDb walk-sound) -80)
        (set! (.VolumeDb run-sound) -3))))

  ;; If speed is less than 1.0 or we're not on the ground, turn down both speakers
  (if (or (< (.Length (v3 (.x *velocity*) 0.0 (.z *velocity*))) 1.0)(= (grounded? (get-kinematic this)) false))
    (do
      (set! (.VolumeDb walk-sound) -80)
      (set! (.VolumeDb run-sound) -80)))
  
  (set-vector! *direction* (.Normalized *direction*)) ;;Normalize the direction vector

  (set! (.y *velocity*) (float (+ (.y *velocity*) (* gravity delta)))) ;;Add gravity every frame

  ;;Instead of setting velocity directly to the direction we want, we smoothly interpolate it by our current acceleration multiplied
  ;;by the frame delta so the acceleration is the same regardless of FPS. This plus the next line is what gives us our
  ;;slidey effect that's common in most first person shooters instead of starting and stopping on a dime.
  
  ;; Multiply our direction by how fast we want to move (walking or running, see speed?), interpolate velocity to our desired
  ;; direction at the rate of our acceleration, grounded w/ input, grounded w/o input, in the air  (see acceleration?) multiplied
  ;; by delta so framerate doesn't matter, replace our current velocity vector with this new slight change in our desired direction
  (set-vector! *velocity* (.LinearInterpolate *velocity* (mult-vector *direction* (speed?)) (float (* (acceleration? this) delta))))

  ;;Apply the velocity just calculated to our kinematic body, but also take the vector returned for the next loop and replace our
  ;; velocity with it. The method MoveAndSlide returns the vector of how much we moved in the real world after application
  ;; By replacing our velocity with this real world information, we will not continue to accelerate when up against a wall
  ;; and when touching the ground and walking off a cliff, the player will behave as expected - starting from 0 velocity in the
  ;; Y axis and building up to terminal velocity.
  (set-vector! *velocity* (.MoveAndSlide (get-kinematic this) *velocity* (v3 0 1 0) false (float 4) (float (Mathf/Deg2Rad 45)) false))

  ;; If you're wondering what the rest of the trash in that method is it's as follows: The force of the move and slide which we plug
  ;; in as velocity, our up vector which is a vector pointing up on the Y axis, plus some arbitrary default values that we don't
  ;; usually need to mess with. I forget what the 2 falses and the float 4 are for, but the Deg2Rad is the max slope angle to allow

  ;; Next up is mouse movement, which is attached to the input loop instead

  (defn input-loop [this delta event]
    (if (= (class event) Godot.InputEventMouseMotion) ;;Mouse Motion loop starts
      (if (= (Input/GetMouseMode) Godot.Input+MouseMode/Captured) ;; If the mouse is captured
        (do
          (.RotateY (get-kinematic this) (Mathf/Deg2Rad (* (.x (.Relative event)) (- sensit)))) ;;Rotate the kinematic body horizontally

          ;; Limit the vertical angle from -80 to positive 80. If the hypothetical *angle* is outside this range, do nothing
          (if (and (> (+ *angle* (* (.y (.Relative event)) (- sensit))) -80) (< (+ *angle* (* (.y (.Relative event)) (- sensit))) 80))
            (do
              (def ^:dynamic *angle* (+ *angle* (* (.y (.Relative event)) (- sensit)))) ;;Update the angle with the successful move for next loop
              (.RotateX (parent this) (Mathf/Deg2Rad (* (.y (.Relative event)) (- sensit))))))))) ;;Perform the vertical rotation on helper

    (if (= (class event) Godot.InputEventMouseButton) ;;If any of the mouse buttons are clicked, capture the mouse
      (Input/SetMouseMode Godot.Input+MouseMode/Captured))
    
    (if (Input/IsActionJustPressed "ui_cancel") ;;If the player hits escape, make the mouse visible 
      (Input/SetMouseMode Godot.Input+MouseMode/Visible))))

(reset-player)
