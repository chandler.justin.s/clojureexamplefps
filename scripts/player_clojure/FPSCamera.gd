extends ClippedCamera
class_name myplayer

var forward = 0.0
var right = 0.0

var angle = 0
var velocity = Vector3()
var grounded = true
var isready = false
var InputDisabled = false

var UpPress = false
var LeftPress = false
var RightPress = false

export (NodePath) var cameraBody

const SENSIT = 0.3
const WALKSPEED = 7.0
const RUNSPEED = 14.0
const ACCEL = 6.0
const DECEL = 12.0
const AIRACCEL = 1.0
const JUMPHEIGHT = 8
const GRAVITY = -9.8 * 2
const SMALLGRAVITY = -9.8 * 2

func _ready():
	pass

func _physics_process(delta):
	var direction = Vector3()
	var aim = self.get_parent().get_parent().get_global_transform().basis
	
	if Input.is_action_pressed("ui_up") or UpPress:
		direction -= aim.z
	if Input.is_action_pressed("ui_down"):
		direction += aim.z
	
	if Input.is_action_pressed("ui_right"):
		direction += aim.x
	if Input.is_action_pressed("ui_left"):
		direction -= aim.x
	
	if(LeftPress):
		self.get_parent().get_parent().rotate_y(deg2rad(5*SENSIT))
		
	if(RightPress):
		self.get_parent().get_parent().rotate_y(deg2rad(-5*SENSIT))
	
	if Input.is_action_just_pressed("ui_accept"):
		if(grounded == true):
			velocity.y = JUMPHEIGHT
	
	direction = direction.normalized()
	
	grounded = self.get_parent().get_parent().is_on_floor()
	
	if not grounded and isready:
		velocity.y += GRAVITY * delta
	else:
		velocity.y += SMALLGRAVITY * delta
	
	var temp = velocity
	temp.y = 0
	
	if temp.length() > 1:
		$WalkSounds.volume_db = -3
		$RunSounds.volume_db = -3
	else:
		$WalkSounds.volume_db = -80
		$RunSounds.volume_db = -80
	
	var speed
	if Input.is_action_pressed("move_sprint") or UpPress:
		speed = RUNSPEED
		$WalkSounds.volume_db = -80
	else:
		speed = WALKSPEED
		$RunSounds.volume_db = -80
	
	if not grounded or InputDisabled:
		$WalkSounds.volume_db = -80
		$RunSounds.volume_db = -80
	
	var target = direction * speed
	
	var acceleration
	if direction.dot(temp) > 0:
		acceleration = ACCEL
	else:
		acceleration = DECEL
	
	if grounded == false:
		acceleration = AIRACCEL
	
	velocity = velocity.linear_interpolate(target, acceleration * delta)
	
	if not InputDisabled:
		velocity = self.get_parent().get_parent().move_and_slide(velocity, Vector3(0,1,0))

func _input(event):
	if event is InputEventMouseMotion and self.current == true and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and not InputDisabled:
		self.get_parent().get_parent().rotate_y(deg2rad(-event.relative.x*SENSIT))
		
		var checkY = -event.relative.y*SENSIT
		if angle + checkY > -80 and angle + checkY < 80:
			angle += checkY
			self.get_parent().rotate_x(deg2rad(checkY))
	if event is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
	if event.is_action_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
