# ClojureExampleFPS

Example first-person shooter project I wrote in Clojure using the brand new ArcadiaGodot API implemented by selfsame!

Personal code can be found in the "game" folder, mainly in player.clj with some side utility functions written in dictionary.clj